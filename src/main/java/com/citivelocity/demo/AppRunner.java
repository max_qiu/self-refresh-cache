package com.citivelocity.demo;

import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AppRunner implements CommandLineRunner {

	@Autowired BookmarkRepository repo;
	@Autowired CacheManager cacheManager;
	
	@Override
	public void run(String... args) throws Exception {
		log.info("A: " + repo.getBookmark("A"));
		log.info("B: " + repo.getBookmark("B"));
		log.info("A: " + repo.getBookmark("A"));
		log.info("B: " + repo.getBookmark("B"));
		log.info("A: " + repo.getBookmark("A"));
		log.info("A: " + repo.getBookmark("A"));
	}

}
