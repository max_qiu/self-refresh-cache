package com.citivelocity.demo;

import java.lang.reflect.Method;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import ch.qos.logback.core.util.ExecutorServiceUtil;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CacheRefreshBeanPostProcessor implements BeanPostProcessor {

	@Autowired CacheManager cacheManager;

	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		Class<?> clazz = AopUtils.getTargetClass(bean);
	    for (Method method : clazz.getDeclaredMethods()) {
	        if( method.isAnnotationPresent(SelfRefresh.class)) {
				ExecutorServiceUtil.newScheduledExecutorService().scheduleWithFixedDelay(new Runnable() {
					public void run() {
						ConcurrentMapCache cache = (ConcurrentMapCache) cacheManager.getCache("bookmarks");
						for(Entry<?, ?> entry : cache.getNativeCache().entrySet()) {
							String name = (String) entry.getKey();
							log.info("Refreshing cache: " + name);
							BookmarkRepository repo = (BookmarkRepository)bean;
							try {
								repo.refresh(name);
								log.info("Refreshed cache: " + repo.getBookmark(name));
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				}, 5, 15, TimeUnit.SECONDS);
	        }
		}
		return bean;
	}

}