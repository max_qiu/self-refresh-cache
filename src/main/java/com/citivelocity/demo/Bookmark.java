package com.citivelocity.demo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data @AllArgsConstructor
public class Bookmark {

	private String name;
    private String url;

}
