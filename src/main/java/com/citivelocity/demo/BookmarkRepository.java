package com.citivelocity.demo;

import java.util.Date;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component @Slf4j
public class BookmarkRepository {

	@Cacheable("bookmarks") @SelfRefresh
	public Bookmark getBookmark(String name) throws InterruptedException {
		log.info("Loading from source: " + name);
		Thread.sleep(1000L);
		return new Bookmark(name, "http://baidu.com " + new Date());
	}

	@CachePut("bookmarks")
	public Bookmark refresh(String name) throws InterruptedException {
		return getBookmark(name);
	}
}